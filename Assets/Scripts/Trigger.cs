﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

   void OnTriggerEnter(Collider collisionInfo)
   {
      if (collisionInfo.gameObject.tag == "standard_snowman")
      {
         Debug.Log("enter");
      }

   }

   void OnTriggerExit(Collider collisionInfo)
   {
      if (collisionInfo.gameObject.tag == "standard_snowman")
      {
         Debug.Log("exit");
      }
   }
}
