﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public GameObject weaponSpawnTiles; // Find the weapon spawn tiles
	public GUIText textHint; // Find and set a GUIText object
	public Texture2D addWeaponIcon;

	// Use this for initialization
	void Start () {
		// hide the weapon spawn tiles at startup
		weaponSpawnTiles.SetActive (false);
		//Clear the text hint
		textHint.text = "";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		// make the outside GUI box
		GUI.Box (new Rect (10, 10, 80, 25), "Actions");

		// GUI buttons
		if (GUI.Button (new Rect (10,40, 80, 72), new GUIContent (addWeaponIcon, "Add a cannon"))) { //, "Add weapons")) {
			weaponSpawnTiles.SetActive (true);
			StartCoroutine(showTextHints("Click on a tile to place the weapon.", 2));
		}
		// This line reads and displays the contents of GUI.tooltip
		GUI.Label (new Rect (100,40,100,20), GUI.tooltip);
	}

	IEnumerator showTextHints (string message, float delay) {
		textHint.text = message;
		yield return new WaitForSeconds (delay);
		textHint.text = "";
	}
	
}
