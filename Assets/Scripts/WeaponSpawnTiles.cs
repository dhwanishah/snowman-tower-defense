﻿using UnityEngine;
using System.Collections;

public class WeaponSpawnTiles : MonoBehaviour {

	private Vector3 clickedTilesPosition;
   public GameObject pre;

	// Use this for initialization
	void Start () {
	
	}
   void Update()
   {
   }

	// Update is called once per frame
	void OnMouseDown () {
		if(Input.GetMouseButtonDown(0)) {
			//clickedTilesPosition = gameObject.transform.position;
			//print("Pressed left click." + gameObject.name);
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			
			if( Physics.Raycast( ray, out hit, 100 ) )
			{
				//Debug.Log( hit.transform.gameObject.name );
            Vector3 pos = GameObject.Find(hit.transform.gameObject.name).transform.position;
            Quaternion rot = GameObject.Find(hit.transform.gameObject.name).transform.rotation;
				print (GameObject.Find(hit.transform.gameObject.name).transform.position);
            Instantiate(pre, pos, rot);
			}
		}
		if(Input.GetMouseButtonDown(1))
			print("Pressed right click.");
		if(Input.GetMouseButtonDown(2))
			print("Pressed middle click.");
	}

	void OnMouseEnter() {
		gameObject.renderer.material.color = Color.red;
	}

	void OnMouseExit() {
		gameObject.renderer.material.color = Color.white;
	}
}
