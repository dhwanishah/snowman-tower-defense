﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	public Transform[] movementPoints;
	private int currentPositionPoint;
	//private int enemeyHealth;
	public int enemyMoveSpeed;
   public ParticleSystem ps;

	// Use this for initialization
	void Start () {
		transform.position = movementPoints [0].position;
		currentPositionPoint = 0;
		//enemeyHealth = 100;
	}
	
	// Update is called once per frame
	void Update () {
      //if (dps > 0)
         //ps.enableEmission = true;
      //else
         ps.enableEmission = false;

		if (currentPositionPoint < movementPoints.Length - 1) { // stop indexOutOfRange
			if (transform.position == movementPoints [currentPositionPoint].position) {
            if(currentPositionPoint != 0) {
               transform.Rotate(0, 270, 0);
            }
				currentPositionPoint++;
			}
		}
		transform.position = Vector3.MoveTowards (transform.position, movementPoints [currentPositionPoint].position, enemyMoveSpeed * Time.deltaTime);
      //transform.Rotate(Vector3.down);
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.name == "Cube") {
			print("hit");
		}
	}
}
